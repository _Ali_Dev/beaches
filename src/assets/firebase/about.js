import {collection,getDocs,getFirestore} from "https://www.gstatic.com/firebasejs/9.0.0/firebase-firestore.js";
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.0.0/firebase-app.js';

const firebaseConfig = {
    apiKey: "AIzaSyDqFdUaWJiZqNu-_A2-P652lqsIuEhTMXM",
    authDomain: "beaches-9bd9f.firebaseapp.com",
    projectId: "beaches-9bd9f",
    storageBucket: "beaches-9bd9f.appspot.com",
    messagingSenderId: "1087731163807",
    appId: "1:1087731163807:web:9c0093fb3e39acd00378aa",
    measurementId: "G-G9WJSKG8VP"
};

export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
const aboutData = async () => {
    const classCollection = collection(db, '/About');

    try {
        const querySnapshot = await getDocs(classCollection);

        querySnapshot.forEach((doc) => {
            const { headline, videoUrl, description, previewImage} = doc.data();
            const classElement = `
                   <div class="col-lg-6">
                       <div class="about-content">
                            <h2>${headline}</h2>
                            <p class="m-0">${description}</p>
                       </div>
                   </div>
                   <div class="col-lg-6">
                       <div class="about-video active">
                            <div class="game">
                                <a href="about-us.html#"><img src="${previewImage}" alt="about"></a>
                            </div> 
                            <div class="video-icon video-hover">
                                <a class="video-popup" href="${videoUrl}">
                                    <i class="zmdi zmdi-play"></i>
                                </a>
                            </div>
                       </div>
                   </div>`;
            document.querySelector('#about-block').innerHTML += classElement;
        });
    } catch (error) {
        console.error('Error getting classes: ', error);
        throw error;
    }
};aboutData();