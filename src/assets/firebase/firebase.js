import {collection,getDocs,getFirestore} from "https://www.gstatic.com/firebasejs/9.0.0/firebase-firestore.js";
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.0.0/firebase-app.js';

const firebaseConfig = {
    apiKey: "AIzaSyDqFdUaWJiZqNu-_A2-P652lqsIuEhTMXM",
    authDomain: "beaches-9bd9f.firebaseapp.com",
    projectId: "beaches-9bd9f",
    storageBucket: "beaches-9bd9f.appspot.com",
    messagingSenderId: "1087731163807",
    appId: "1:1087731163807:web:9c0093fb3e39acd00378aa",
    measurementId: "G-G9WJSKG8VP"
};

export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);

const getClasses = async () => {
    const classCollection = collection(db, '/Classes');

    try {
        const querySnapshot = await getDocs(classCollection);

        querySnapshot.forEach((doc) => {
            const { title, teacher, imgUrl, time } = doc.data();
            const classElement = `
                <div class="col-lg-4 col-md-6">     
                    <div class="single-class">
                        <div class="single-img">
                            <a href="class.html"><img src="${imgUrl}" alt="class"></a>
                            <div class="gallery-icon">
                                <a class="image-popup" href="${imgUrl}">
                                    <i class="zmdi zmdi-zoom-in"></i>
                                </a>   
                            </div>
                        </div>
                        <div class="single-content">
                            <h3><a href="class.html">${title}</a></h3>
                            <ul>
                                <li><i class="zmdi zmdi-face"></i>${teacher}</li>
                                <li><i class="zmdi zmdi-alarm"></i>${time}</li>
                            </ul>
                        </div>
                    </div>
                </div>`;
            document.querySelector('#club-classes').innerHTML += classElement;
        });
    } catch (error) {
        console.error('Error getting classes: ', error);
        throw error;
    }
};getClasses();

const getSliders = async () => {
    const sliderCollection = collection(db, '/Sliders');

    try {
        const querySnapshot = await getDocs(sliderCollection);

        querySnapshot.forEach((doc) => {
            const { title, description, textLine, bg, btnUrl } = doc.data();
            // Create the HTML for the slider
            const sliderElement = `
               <div class="single-slide" style="background-image: url(${bg});">
                      <div class="slider-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="text-content-wrapper">
                                        <div class="text-content text-start">
                                            <h5>${textLine}</h5>
                                            <h1>${title}</h1>
                                            <p>${description}</p>
                                            <a class="banner-btn" href="${btnUrl}" data-text="read more"><span>read more</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                   </div> 
                </div>`;
            document.querySelector('#sl-container').innerHTML += sliderElement;
        });
    } catch (error) {
        console.error('Error getting sliders: ', error);
        throw error;
    }
}; getSliders();

const getPrices = async () => {
    const sliderCollection = collection(db, '/Prices');

    try {
        const querySnapshot = await getDocs(sliderCollection);

        querySnapshot.forEach((doc) => {
            const { packageName, price, option1, option2, option3, option4 } = doc.data();
            const sliderElement = `
               <div class="col-lg-4 col-md-6">
                   <div class="single-table text-center">
                        <div class="table-head">
                            <h2>${packageName}</h2>
                            <h1>${price}<span>/month</span></h1>
                        </div>
                        <div class="table-body">
                            <ul>
                                <li>${option1}</li>
                                <li>${option2}</li>
                                <li>${option3}</li>
                                <li>${option4}</li>
                            </ul>
                            <a href="index.html#">get started</a>
                        </div>
                   </div>
                </div>`;
            document.querySelector('#sc-prices').innerHTML += sliderElement;
        });
    } catch (error) {
        console.error('Error getting sliders: ', error);
        throw error;
    }
}; getPrices();