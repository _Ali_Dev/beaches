import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.0.0/firebase-app.js';
import { getFirestore, collection, addDoc, serverTimestamp } from 'https://www.gstatic.com/firebasejs/9.0.0/firebase-firestore.js';

const firebaseConfig = {
    apiKey: "YOUR_API_KEY",
    authDomain: "YOUR_AUTH_DOMAIN",
    projectId: "YOUR_PROJECT_ID",
    storageBucket: "YOUR_STORAGE_BUCKET",
    messagingSenderId: "YOUR_MESSAGING_SENDER_ID",
    appId: "YOUR_APP_ID",
    measurementId: "YOUR_MEASUREMENT_ID"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

const contactForm = document.getElementById('contact-form');

contactForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    const name = contactForm.querySelector('input[name="user_name"]').value;
    const email = contactForm.querySelector('input[name="user_email"]').value;
    const message = contactForm.querySelector('textarea[name="user_message"]').value;

    console.log('Name:', name);
    console.log('Email:', email);
    console.log('Message:', message);

    try {
        await addDoc(collection(db, 'contactSubmissions'), {
            name: name,
            email: email,
            message: message,
            timestamp: serverTimestamp()
        });

        console.log('Form submitted successfully');
        contactForm.reset();
    } catch (error) {
        console.error('Error submitting form: ', error);
    }
});

/*
Waiting for response from Google cloud support (couldn't create new account)

function initMap() {
    const map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 8,
    });
}
document.addEventListener('DOMContentLoaded', initMap);
 */